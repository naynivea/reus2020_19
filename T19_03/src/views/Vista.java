/**
 * 
 */
package views;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;





/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Vista extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 685, 793);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitol = new JLabel("Miniencuesta");
		lblTitol.setForeground(Color.RED);
		lblTitol.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblTitol.setBounds(268, 13, 146, 16);
		contentPane.add(lblTitol);
		
		JLabel lblSistema = new JLabel("Elije un sistema operativo");
		lblSistema.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSistema.setBounds(12, 59, 189, 16);
		contentPane.add(lblSistema);
		
		JRadioButton rdbtn1 = new JRadioButton("Windows");
		rdbtn1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtn1.setBounds(22, 92, 127, 25);
		rdbtn1.setActionCommand("Windows");
		rdbtn1.setSelected(true);
		contentPane.add(rdbtn1);
		
		JRadioButton rdbtn2 = new JRadioButton("Linux");
		rdbtn2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtn2.setBounds(22, 122, 127, 25);
		rdbtn2.setActionCommand("Linux");
		contentPane.add(rdbtn2);
		
		JRadioButton rdbtn3 = new JRadioButton("Mac");
		rdbtn3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtn3.setBounds(22, 152, 127, 25);
		rdbtn3.setActionCommand("Mac");
		contentPane.add(rdbtn3);
		
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(rdbtn1);
		bgroup.add(rdbtn2);
		bgroup.add(rdbtn3);
		
		JLabel lblEspecialidad = new JLabel("Elije tu especialidad");
		lblEspecialidad.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEspecialidad.setBounds(12, 207, 146, 16);
		contentPane.add(lblEspecialidad);
		
		JCheckBox checkBox1 = new JCheckBox("Programaci\u00F3n");
		checkBox1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		checkBox1.setBounds(22, 232, 127, 25);
		contentPane.add(checkBox1);
		
		JCheckBox checkBox2 = new JCheckBox("Dise\u00F1o gr\u00E1fico");
		checkBox2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		checkBox2.setBounds(22, 261, 127, 25);
		contentPane.add(checkBox2);
		
		JCheckBox checkBox3 = new JCheckBox("Administraci\u00F3n");
		checkBox3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		checkBox3.setBounds(22, 291, 127, 25);
		contentPane.add(checkBox3);
		
		JSlider slider = new JSlider();
		slider.setValue(10);
		slider.setMaximum(10);
		slider.setBounds(33, 369, 200, 26);
		contentPane.add(slider);
		
		JLabel lblHoras = new JLabel("Horas dedicadas en el ordenador");
		lblHoras.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblHoras.setBounds(12, 340, 240, 16);
		contentPane.add(lblHoras);
		
		JLabel lblResultadoS = new JLabel("Sistema operativo: ");
		lblResultadoS.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblResultadoS.setBounds(12, 528, 181, 16);
		contentPane.add(lblResultadoS);
		
		JLabel lblResultadoE = new JLabel("Especialidad:");
		lblResultadoE.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblResultadoE.setBounds(12, 557, 114, 16);
		contentPane.add(lblResultadoE);
		
		JLabel lblResultadoH = new JLabel("Horas dedicadas:");
		lblResultadoH.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblResultadoH.setBounds(12, 586, 146, 16);
		contentPane.add(lblResultadoH);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(52, 436, 97, 25);
		contentPane.add(btnEnviar);
		
		JLabel lblResultadoSistema = new JLabel("");
		lblResultadoSistema.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblResultadoSistema.setBounds(182, 529, 142, 16);
		contentPane.add(lblResultadoSistema);
		
		JLabel lblResultadoEspecialidad = new JLabel("");
		lblResultadoEspecialidad.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblResultadoEspecialidad.setBounds(182, 557, 473, 16);
		contentPane.add(lblResultadoEspecialidad);
		
		JLabel lblResultadoHoras = new JLabel("");
		lblResultadoHoras.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblResultadoHoras.setBounds(182, 586, 142, 16);
		contentPane.add(lblResultadoHoras);	
		
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String especialidad = "";
				if(checkBox1.isSelected() == true) {
					especialidad += "Programación ";
				}
				if(checkBox2.isSelected() == true) {
					especialidad += "Diseño gráfico ";
				}
				if(checkBox3.isSelected() == true) {
					especialidad += "Administración ";
				}
				lblResultadoSistema.setText(bgroup.getSelection().getActionCommand());
				lblResultadoEspecialidad.setText(especialidad);
				lblResultadoHoras.setText(String.valueOf(slider.getValue()));
			}
		};
		btnEnviar.addActionListener(al);
	}
}
