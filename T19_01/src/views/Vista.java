/**
 * 
 */
package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Escribre un nombre para saludar");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(80, 68, 266, 16);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(40, 122, 337, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btn1 = new JButton("\u00A1Saludar!");
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "�Hola " + textField.getText() + "!");
			}
		};
		btn1.addActionListener(al);
		btn1.setBounds(156, 180, 97, 25);
		contentPane.add(btn1);
	}
}
