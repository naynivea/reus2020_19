import views.Vista;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class SaludadorApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vista vista = new Vista();

		vista.setVisible(true);
	}

}
