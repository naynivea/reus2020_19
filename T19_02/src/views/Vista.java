/**
 * 
 */
package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 568, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Escribe el titulo de una pelicula");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(39, 78, 225, 16);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(49, 119, 186, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Peliculas");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(374, 79, 105, 16);
		contentPane.add(lblNewLabel_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(323, 119, 156, 22);
		contentPane.add(comboBox);
		
		JButton btnNewButton = new JButton("A\u00F1adir");
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				comboBox.addItem(textField.getText());
			}
		};
		btnNewButton.addActionListener(al);
		btnNewButton.setBounds(86, 164, 97, 25);
		contentPane.add(btnNewButton);
		
	}
}
